# aevol utilities

Utilities (mostly Python) I use around [aevol](https://gitlab.inria.fr/aevol/aevol).

The version of the notebooks that I used for my PhD manuscript are in branch [phd](https://gitlab.inria.fr/tgrohens/aevol-utilities/-/tree/phd) of the repo.
