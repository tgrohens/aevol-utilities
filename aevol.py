import numpy as np
import datetime
import pathlib
import execo
import shutil
from typing import List, Dict, Callable

def right_now() -> str:
  return datetime.datetime.now().strftime('[%Y/%m/%d %H:%M:%S]')

class AevolExperiment:

  def __init__(self,
               exp_path: str,
               exp_name: str,
               aevol_exec_path: str,
               run_if_debug: bool = False) -> None:

    # Experiment name
    self.exp_path = pathlib.Path(exp_path)
    self.exp_name = exp_name
    self.replicate_paths = []
    self.nb_replicas = 0

    # Aevol executables
    self.aevol_exec_path = pathlib.Path(aevol_exec_path)

    # Experimental setup, initialized in setup_experiment()
    self.init_indiv_path = None

    # aevol_run parameters, initialized in run_aevol_run()
    self.nb_generations = None
    self.nb_threads = None
    self.run_as_aevol_6 = None

    # Runner-level flags
    self.run_if_debug = run_if_debug

    # Running processes
    self.processes = {} # {'executable_name': [processes]}

    # Internal state
    self.has_created_folders = False
    self.has_run_aevol_create = False
    self.has_started_aevol_run = False

    # RNG used for initializing the seed for each replica
    self.rng = np.random.default_rng()


  def reload(self, reload_name: str, run_as_aevol_6: bool = None) -> None:
    # Sanity checks
    if not self.exp_path.joinpath(reload_name).exists():
      print(f"There is no experiment named '{reload_name}' "
            f"in folder '{self.exp_path}'.")
      return

    exp_name = '_'.join(reload_name.split('_')[:-1])

    if exp_name != self.exp_name:
      print(f"Recovered experiment name '{exp_name}' does not match "
            f"original name '{self.exp_name}'.")
      return


    self.replicate_paths = (
        sorted([d for d in self.exp_path.joinpath(reload_name).iterdir()
                  if d.name.startswith("rep")]))
    self.nb_replicas = len(self.replicate_paths)

    self.run_as_aevol_6 = run_as_aevol_6

    # Check if replicate folders have been created
    if len(self.replicate_paths) == 0:
      return

    self.has_created_folders = True

    # Check if aevol_create has been run
    if not self.replicate_paths[0].joinpath('exp_setup').exists():
      return

    self.has_run_aevol_create = True

    # Check if aevol_run has been run
    if self.get_last_generation(replicate=0) == 0:
      return

    self.has_started_aevol_run = True


  def exec_path(self, name: str) -> pathlib.Path:
    return self.aevol_exec_path.joinpath(name).expanduser()


  def create_experiment_folders(self,
                                main_exp_path: pathlib.Path,
                                nb_replicas: int) -> None:

    main_exp_path.mkdir(exist_ok=False) # Should not fail but let's make sure

    # Create folders for each of the replicas
    # Folder names: exp_name_rep#
    if nb_replicas is None:
      nb_replicas = 1

    self.nb_replicas = nb_replicas
    self.replicate_paths = [main_exp_path.joinpath(f'rep{i_rep}')
                              for i_rep in range(nb_replicas)]

    for rep_path in self.replicate_paths:
        rep_path.mkdir(exist_ok=False)


  def populate_experiment_folders(self,
                                  param_lines: List[str],
                                  **kwargs) -> None:

    # Populate each folder with its parameter file,
    # and optionally its chromosome (initial individual)
    for i_rep, rep_path in enumerate(self.replicate_paths):

      # Generate a random seed for this replica
      seed = self.rng.integers(1000000000)

      rep_params = self.replace_params(param_lines,
                                       {'SEED': f'{seed}',
                                        'STRAIN_NAME': f'{self.exp_name}_rep{i_rep}',
                                        **kwargs})

      rep_param_path = rep_path.joinpath('param.in')

      # Write the param.in file
      # Will raise FileExistsError should there already be a file
      with rep_param_path.open('x') as rep_param_file:
        for line in rep_params:
          rep_param_file.write(line)

      # Copy the initial individual into the folder, if there is one
      if self.init_indiv_path:
        shutil.copy(self.init_indiv_path, rep_path)


  def replace_params(self,
                     param_lines: List[str],
                     new_params: Dict[str, str]) -> List[str]:
    return [self.replace_param_line(line, new_params) for line in param_lines]


  # Function to be called on each line
  def replace_param_line(self, line: str, new_params: Dict[str, str]) -> str:
    if line.isspace() or line.startswith('#'):
      return line

    param_name = line.split()[0]

    if param_name in new_params.keys():
      indent = line.count(' ')
      if new_params[param_name]:
        new_line = param_name + (indent * ' ') + new_params[param_name] + '\n'
      else: # disable the parameter
        new_line = '# ' + param_name + '\n'
      return new_line

    return line


  def check_if_debug(self) -> bool:

    command = f'{self.exec_path("aevol_run")} --version'

    aevol_run_proc = execo.Process(command)
    aevol_run_proc.run()

    output = aevol_run_proc.stdout.split("\n")

    return ("debug" in output[0].lower())


  def setup_experiment(self,
                       base_param_path: str,
                       init_indiv_path: str = None,
                       nb_replicas: int = None,
                       **kwargs) -> None:

    if self.has_created_folders:
      print("Replica folders have already been created for this experiment.")
      return

    if init_indiv_path:
      self.init_indiv_path = pathlib.Path(init_indiv_path)

    # Look up the main folder name and check it doesn't already exist
    today = datetime.datetime.now().strftime('%Y%m%d')
    main_exp_path = self.exp_path.joinpath(f'{self.exp_name}_{today}')
    if main_exp_path.is_dir():
      print(f"An experiment named '{self.exp_name}' already exists. Aborting.")
      return

    # Check if the Aevol we're running is in Debug mode, and exit
    # if we don't want it to be.
    if not self.run_if_debug:
      if self.check_if_debug():
        print("The aevol_run executable is in Debug mode, but the "
              "'run_if_debug' option is set to False. Aborting.")
        return

    # Load the baseline parameter file
    with open(base_param_path, 'r') as param_file: # Will raise FileNotFoundError if there is no file
        param_lines = param_file.readlines()

    # Create the folder structure for the experiment
    self.create_experiment_folders(main_exp_path, nb_replicas)

    # Create the param.in file associated with each replica
    self.populate_experiment_folders(param_lines, **kwargs)

    self.has_created_folders = True


  def run_aevol_create(self,
                       run_sequentially: bool = False,
                       init_basal_sc: float = None) -> None:

    if not self.has_created_folders:
      print('Replica folders have not yet been created for this experiment.')
      return

    if self.has_run_aevol_create:
      print('Replica populations have already been created for this experiment.')
      return

    sep_string = '-' * 100 + '\n'

    is_seq_str = 'in parallel'
    if run_sequentially:
      is_seq_str = 'sequentially'
    print(f'Running aevol_create processes {is_seq_str}.\n')

    self.processes['aevol_create'] = []

    for i_rep, rep_path in enumerate(self.replicate_paths):

      init_indiv_arg = ''
      if self.init_indiv_path is not None:
        init_indiv_arg = f'-C {self.init_indiv_path.name}'

      init_basal_sc_arg = ''
      if init_basal_sc is not None:
        init_basal_sc_arg = f'--basal-supercoiling {init_basal_sc}'

      aevol_create_path = self.exec_path('aevol_create')
      aevol_create_command = ' '.join([f'"{aevol_create_path}"',
                                        init_indiv_arg,
                                        init_basal_sc_arg])

      handler = AevolProcessLifecycleHandler(experiment=self,
          exec_name='aevol_create', full_command=aevol_create_command,
          rep_number=i_rep, run_sequentially=False)

      full_command = f'cd "{rep_path}"; {aevol_create_command}'

      aevol_create_proc = execo.Process(full_command, shell=True,
                                        lifecycle_handlers=[handler])

      if run_sequentially:
        aevol_create_proc.run()
        print(aevol_create_proc.stdout)
        print(sep_string)
      else:
        aevol_create_proc.start()


      if aevol_create_proc.exit_code == 1:
        raise ValueError(f'{rep_path}: aevol_create failed with output:\n'
                            f'{aevol_create_proc.stderr}')

      self.processes['aevol_create'].append(aevol_create_proc)

    if not run_sequentially:
      for p in self.processes['aevol_create']:
        p.wait()
      for i_rep, p in enumerate(self.processes['aevol_create']):
        print(f'Output for replica {i_rep}:\n')
        print(p.stdout)
        print(sep_string)

      print(f"{right_now()} All aevol_create processes finished!\n")

    self.has_run_aevol_create = True


  def get_last_generation(self, replicate: int) -> int:
    last_gener_path = self.replicate_paths[replicate].joinpath("last_gener.txt")

    with open(last_gener_path, "r") as last_gener_file:
      last_generation = int(last_gener_file.readlines()[0])

    return last_generation


  def make_aevol_run_command(self,
                             restart: bool = False,
                             nb_threads: int = None,
                             nb_gen: int = None,
                             replicate: int = None) -> str:

    # Number of generations
    nb_gen_arg = ''

    if restart and (self.nb_generations is not None or nb_gen is not None):

      last_generation = self.get_last_generation(replicate)

      if self.nb_generations is not None:
        nb_remaining_generations = self.nb_generations - last_generation
      else: # nb_gen is not None
        nb_remaining_generations = nb_gen - last_generation

      if nb_remaining_generations <= 0:
        raise ValueError('No generations remain to run.')

      nb_gen_arg = f'-n {nb_remaining_generations}'

    else:
      if self.nb_generations is not None:
        nb_gen_arg = f'-n {self.nb_generations}'

    # Number of threads
    nb_threads_arg = ''
    if not restart:
      if self.nb_threads is not None:
        nb_threads_arg = f'-p {self.nb_threads}'
    else: # Override if we restart
      if nb_threads is not None:
        nb_threads_arg = f'-p {nb_threads}'
      elif self.nb_threads is not None:
        nb_threads_arg = f'-p {self.nb_threads}'

    # Run as aevol 6?
    aevol_6_arg = ''
    if self.run_as_aevol_6:
      aevol_6_arg = '--aevol_6'

    # Redirect standard output
    output_filename = 'aevol_output.txt'
    if not restart:
      output_redirection = f'> {output_filename} 2>&1'
    else: # append instead of creating the file
      output_redirection = f'>> {output_filename} 2>&1'

    # Final command
    aevol_run_path = self.exec_path('aevol_run')
    aevol_run_command = ' '.join([f'"{aevol_run_path}"', nb_gen_arg,
                                  nb_threads_arg, aevol_6_arg,
                                  output_redirection])

    return aevol_run_command


  def run_aevol_run(self,
                    nb_generations: int = None,
                    nb_threads: int = None,
                    run_sequentially: bool = False,
                    run_as_aevol_6: bool = False) -> None:

    if not self.has_run_aevol_create:
      print('Populations have not yet been created for this experiment.')
      return

    if self.has_started_aevol_run:
      print('Evolutionary runs have already been started for this experiment.')
      return

    self.nb_generations = nb_generations
    self.nb_threads = nb_threads
    self.run_as_aevol_6 = run_as_aevol_6

    self.processes['aevol_run'] = []

    for i_rep, rep_path in enumerate(self.replicate_paths):

      aevol_run_command = self.make_aevol_run_command()
      full_command = f'cd "{rep_path}"; {aevol_run_command}'

      handler = AevolProcessLifecycleHandler(experiment=self,
          exec_name='aevol_run', full_command=aevol_run_command,
          rep_number=i_rep, run_sequentially=run_sequentially)

      aevol_process = execo.Process(full_command, shell=True,
                                    lifecycle_handlers=[handler])

      self.processes['aevol_run'].append(aevol_process)

    is_seq_str = 'in parallel'
    if run_sequentially:
      is_seq_str = 'sequentially'
    print(f'Running aevol_run processes {is_seq_str}.\n')

    if run_sequentially:
      self.run_next_process(exec_name='aevol_run', rep_number=0)
    else:
      for p in self.processes['aevol_run']:
        p.start()

    self.has_started_aevol_run = True


  def restart_runs(self,
                   new_nb_threads: int = None,
                   new_nb_gen: int = None,
                   run_sequentially: bool = False) -> None:

    if not self.has_started_aevol_run:
      print('Evolutionary runs have not yet been started for this experiment.')
      return

    new_processes = []

    for i_rep in range(self.nb_replicas):
      need_new_process = True

      # If we just reloaded an experiment, 'aevol_run' is not in self.processes
      if 'aevol_run' in self.processes:
        process = self.processes['aevol_run'][i_rep]
        if process.ok:
          need_new_process = False
          print(f'Replicate #{i_rep} is still running, or has finished normally'
                f' ({self.get_last_generation(i_rep)} generations elapsed).')
          new_processes.append(process)

      if need_new_process:
        rep_path = self.replicate_paths[i_rep]

        restart_command = self.make_aevol_run_command(restart=True,
          nb_threads=new_nb_threads, nb_gen=new_nb_gen, replicate=i_rep)

        full_command = f'cd "{rep_path}"; {restart_command}'

        handler = AevolProcessLifecycleHandler(experiment=self,
          exec_name='aevol_run', full_command=full_command,
          rep_number=i_rep, run_sequentially=run_sequentially)

        aevol_process = execo.Process(full_command, shell=True,
                                      lifecycle_handlers=[handler])

        new_processes.append(aevol_process)

    self.processes['aevol_run'] = new_processes

    is_seq_str = 'in parallel'
    if run_sequentially:
      is_seq_str = 'sequentially'
    print(f'Restarting aevol_run processes {is_seq_str}.\n')

    if run_sequentially:
      self.run_next_process(exec_name='aevol_run', rep_number=0)
    else:
      for p in self.processes['aevol_run']:
        if not p.finished_ok:
          p.start()


  def run_post_treatment(self,
                         name: str,
                         args: List[str],
                         rep_args_fun: Callable[..., List[str]] = None,
                         run_sequentially: bool = False,
                         write_output_to_file: bool=False) -> None:

    if not self.has_started_aevol_run:
      print('Evolutionary runs have not been started yet in this experiment.')
      return

    # if name in self.processes:
    #   # Maybe do something smarter here
    #   print(f'Post-treatment {name} has already been started.')
    #   return

    full_name = f'aevol_post_{name}'

    post_treatment_path = self.exec_path(full_name)

    if not post_treatment_path.exists():
      print(f"No post-treatment named '{name}' exists. Aborting.")
      return

    self.processes[full_name] = []

    # Create all processes
    for i_rep, rep_path in enumerate(self.replicate_paths):

      output_redirection = ''
      if write_output_to_file:
        output_redirection = f'> {name}_output.txt'

      rep_args = ''
      if rep_args_fun:
        rep_args = rep_args_fun(rep_path)

      post_treatment_command = ' '.join([f'"{post_treatment_path}"',
                                         *rep_args, *args, output_redirection])

      full_command = f'cd "{rep_path}"; {post_treatment_command}'
      handler = AevolProcessLifecycleHandler(experiment=self,
          exec_name=full_name, full_command=full_command, rep_number=i_rep,
          run_sequentially=run_sequentially)

      post_treatment_process = execo.Process(full_command, shell=True,
                                             lifecycle_handlers=[handler])

      self.processes[full_name].append(post_treatment_process)

    if run_sequentially:
      # Start the first process
      self.run_next_process(exec_name=full_name, rep_number=0)
    else:
      # Start all processes
      for p in self.processes[full_name]:
        p.start()


  def run_next_process(self, exec_name: str, rep_number: int):

    if rep_number == self.nb_replicas:  # We're done
      print(f"{right_now()} All replicas of '{exec_name}' have finished running.")
      return

    # Skip already finished processes
    if not self.processes[exec_name][rep_number].finished_ok:
      self.processes[exec_name][rep_number].start()
    else:
      self.run_next_process(exec_name=exec_name, rep_number=rep_number+1)



class AevolProcessLifecycleHandler(execo.ProcessLifecycleHandler):

  def __init__(self,
               experiment: AevolExperiment,
               exec_name: str,
               full_command: str,
               rep_number: int,
               run_sequentially: bool) -> None:
    self.experiment = experiment
    self.exec_name = exec_name
    self.full_command = full_command
    self.rep_number = rep_number
    self.run_sequentially = run_sequentially

  def start(self, process):
    print(f"{right_now()} Replicate #{self.rep_number}: Starting "
          f"'{self.full_command}' in directory "
          f'"{self.experiment.replicate_paths[self.rep_number]}"...\n')


  def end(self, process):
    if process.finished_ok:
      print(f'{right_now()} Replicate #{self.rep_number}: '
            f'{self.exec_name} finished successfully.\n')
    else:
      print(f'{right_now()} Replicate #{self.rep_number}: '
      f'{self.exec_name} finished abnormally (error {process.exit_code}).\n')

    if self.run_sequentially:
      self.experiment.run_next_process(self.exec_name, self.rep_number+1)


  def reset(self, process):
    pass


def get_lineage_name(rep_path):
    rep_path = pathlib.Path(rep_path)
    lineage_files = list(rep_path.glob('lineage-*.ae'))
    if len(lineage_files) == 1:
        return [str(lineage_files[0].name)]
    else:
        raise ValueError(f'{len(lineage_files)} lineage file(s) '
                          'found (expected one). Aborting.')